import unittest
from desafio_datas_pedro import retorna_datas, valida_data


class TestClassificaSenha (unittest.TestCase):

    def test_anos_1900_1990_2142(self):
        # O retorno e o mesmo para as respectivas datas
        ano1 = retorna_datas(1900)
        ano2 = retorna_datas(1990)
        ano3 = retorna_datas(2142)
        retorno = ('Carnaval: 27/2', 'Sexta-feira Santa: 13/4', 'Páscoa: 15/4', 'Corpus Christi: 14/6')
        anos = [ano1, ano2, ano3]
        for ano in anos:        
            self.assertEqual(ano, retorno)
        return ano

    def test_retorno_ano_errado(self):
        ano = retorna_datas(2019)
        retorno = ('Carnaval: 27/2', 'Sexta-feira Santa: 13/4', 'Páscoa: 15/4', 'Corpus Christi: 14/6')
        teste = False if ano != retorno else True
        self.assertFalse(teste)

    def test_ano_invalido(self):
       
        ano1 = valida_data(10000)
        ano2 = valida_data(1582)
        anos = [ano1, ano2]
        for ano in anos:
            self.assertFalse(ano)

    def test_integrado(self):
        
        # Utilizo o modulo valida_data para retornar apenas as datas validas
        numeros = [0,-10,1900, 1400, 1990, 12000, 2142]
        numeros_validos = [numero for numero in numeros if valida_data(numero) != False]

        # Utilizo o modulo para pegar um retorno padrao para as respectivas datas
        retorno_esperado = self.test_anos_1900_1990_2142()
        
        # Utilizo o modulo retorna_datas para testar se as datas batem com o retorno esperado
        for teste in numeros_validos:
            teste = retorna_datas(teste)
            self.assertEqual(teste, retorno_esperado)


if __name__ == '__main__':
    unittest.main()
