import datetime
from datetime import timedelta, date
from sys import argv

def valida_data(ano_valida):
    return ano_valida if ano_valida >= 1583 and ano_valida <= 9999 else False   
   

def retorna_datas(ano):
    try:
        a = ano % 19
        b = ano / 100
        b = int(b)
        c = ano % 100
        d = b / 4
        d = int(d)
        e = b % 4
        f = (b + 8) / 25
        f = int(f)
        g = (b - f + 1) / 3
        g =  int(g)
        h = (19 * a + b - d - g + 15) % 30
        i = c / 4
        i = int(i)
        k = c % 4
        L = (32 + 2 * e + 2 * i - h - k) % 7
        m = (a + 11 * h + 22 * L) / 451
        m = int(m)

        mes = (h + L - 7 * m + 114) / 31
        mes = int(mes)
        dia = 1+ (h + L - 7 * m + 114)% 31

        sextasanta = datetime.date(ano, mes, dia) - timedelta(2)
        carnaval = datetime.date(ano, mes, dia) - timedelta(47)
        corpus = datetime.date(ano, mes, dia) + timedelta(60)

        carnaval = f'Carnaval: {carnaval.day}/{carnaval.month}'
        sextasanta = f'Sexta-feira Santa: {sextasanta.day}/{sextasanta.month}'
        pascoa = f'Páscoa: {dia}/{mes}'
        corpus = f'Corpus Christi: {corpus.day}/{corpus.month}'

        return carnaval, sextasanta, pascoa, corpus
    except:
           print("Digite um ano entre 1583 e 9999")
                   

if __name__ == "__main__":
    ano_valida = int(argv[1])
    ano = (valida_data(ano_valida))
    print(retorna_datas(ano))
    

